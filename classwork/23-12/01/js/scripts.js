const userYearOfBirth = Number(prompt ('Введите год рождения пользователя'));
const userMonthOfBirth = Number(prompt ('Введите месяц рождения пользователя'));
const userDayOfBirth = Number(prompt ('Введите день рождения пользователя'));

const nowYear = 2019;
const nowMonth = 12;
const nowDay = 23;

if (
    isNaN(userYearOfBirth ) || userYearOfBirth ===0 ||
    isNaN(userMonthOfBirth) || userYearOfBirth ===0 ||
    isNaN(userDayOfBirth) || userYearOfBirth ===0
) {
    alert ('Смотри что вводишь!');
    throw new Error ('Ошибка ввода.');
}

if ( userYearOfBirth > nowYear) {
    alert ('Года должен быть меньше введенного')
    throw new Error ('Ошибка')
}

// Пример линейного комментария
/**
 * приемер
 * многосточного
 * комментария
 */

let resultYear;
let resultMonth;
let resultDay;

if (nowMonth >= userMonthOfBirth && nowDay>=userDayOfBirth) {
    resultYear = nowYear - userYearOfBirth;
    resultMonth = nowMonth -userMonthOfBirth;
    resultDay =nowDay - userDayOfBirth;
}

else if (nowDay >= userDayOfBirth && nowMonth < userMonthOfBirth) {
    resultYear = nowYear - userYearOfBirth - 1;
    resultMonth = nowMonth + 12 - userMonthOfBirth;
    resultDay = nowDay - userDayOfBirth;
}

console.log (resultYear, resultMonth, resultDay);


